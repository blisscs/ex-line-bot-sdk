FROM elixir:1.6.6
RUN mix local.hex --force
RUN mix local.rebar --force
RUN mkdir -p /ex-line-bot-sdk
ADD . /ex-line-bot-sdk
WORKDIR /ex-line-bot-sdk
