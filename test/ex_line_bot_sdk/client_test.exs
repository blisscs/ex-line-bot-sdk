defmodule ExLineBotSdk.ClientTest do
  use ExUnit.Case, async: true

  defmodule SampleLineClient do
    @moduledoc false

    @channel_access_token "Sample Line Client Token"
    @channel_secret "test_secret"

    alias ExLineBotSdk.LineClient

    use LineClient
  end

  defmodule Check do
    def check(_), do: true
  end

  defmodule MyPlugPipeline do
    @moduledoc false

    use Plug.Builder

    alias Plug.Parsers
    alias PlugSetRequestRawData

    plug(PlugSetRequestRawData, %{check: &Check.check/1})

    plug(
      Parsers,
      parsers: [:urlencoded, :multipart, :json],
      pass: ["*/*"],
      json_decoder: Poison
    )
  end

  test "channel_access_token/1" do
    assert SampleLineClient.channel_access_token() == "Sample Line Client Token"
  end

  test "verify/1 return {:ok, conn} when valid x-line-signature" do
    use Plug.Test

    body = "{ \"hello\": \"world\" }"

    conn =
      conn(:post, "/", body)
      |> put_req_header("x-line-signature", "R4ijDU6G9pjENk6WltpJYvRpBzRfYT971uAES+ucl3c=")
      |> MyPlugPipeline.call(%{})

    assert SampleLineClient.verify(conn) == {:ok, conn}
  end

  test "verify/1 return {:ok, conn} when valid x-line-signature also when data is empty string" do
    use Plug.Test

    body = ""

    conn =
      conn(:post, "/", body)
      |> put_req_header("x-line-signature", "9/m9R/uYcze1eW/cH9uboiHQ1TloFL/K+VIfQ/2JJ/0=")
      |> MyPlugPipeline.call(%{})

    assert SampleLineClient.verify(conn) == {:ok, conn}
  end

  test "signature_for_request_should_be/1 return signature to be compared" do
    use Plug.Test

    body = "{something: 5}"

    conn =
      conn(:post, "/", body)
      |> put_req_header("x-line-signature", "P0HDrGiTCnndwfgDOOh79904UiDVRq48CKn2IkJWmsw=")
      |> MyPlugPipeline.call(%{})

    assert "rkC8WKMjlme49J+tqeQdgMrxnAtyZoWH8+u7jDtkoS0=" ==
             SampleLineClient.signature_for_request_should_be(conn)
  end

  test "verify return {:error, \"Line Request is not verified.\"} when invalid x-line-signature" do
    use Plug.Test

    body = """
    {something: "b"}
    """

    conn =
      conn(:post, "/", body)
      |> put_req_header("x-line-signature", "r9LTdsux+AbcwMB8ZDPgmDgLtjTDQoI12pWuG27GHWs=")
      |> MyPlugPipeline.call(%{})

    assert SampleLineClient.verify(conn) == {:error, "Line Request is not verified."}
  end
end
