defmodule ExLineBotSdk do
  @moduledoc """
  `ExLineBotSdk` provides a Line's Client library, that is very easy to use.
  This library also allows single mix project to have many Line's clients per mix project.

  ## Installation

  Add `:ex_line_bot_sdk` to `deps()` in `mix.exs` file

      defp deps do
        [
          ...,
          {:ex_line_bot_sdk, "~> 0.0.8"}
        ]
      end

  ## Setting up Elixir Line Client

  It is very easy to setup a line client in your project.
  Below is an example of how to create a new client for your elixir application.

      defmodule MyApp.LineClient do
        @channel_access_token "[Your Line Access Token Here]"
        @channel_secret "[Your Channel Secret Here]"

        use ExLineBotSdk.LineClient
      end

  Now this module will provide all neccessary functions for interacting to Line.
  To see all the functions that client library provides please checkout `ExLineBotSdk.LineClient`.
  """
end
