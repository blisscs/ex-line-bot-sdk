defmodule ExLineBotSdk.LineClient do
  @moduledoc """
  `ExLineBotSdk.LineClient` provides client's functionalities to connect Line messaging api endpoint.

  To setup a line's client library in your project add `use ExLineBotSdk.LineClient`
  and set module attribute `channel_access_token` in your module like the code example shown below.

      defmodule MyApp.LineClient do
        @channel_access_token "[Your Line Access Token Here]"
        @channel_secret "[Your Channel Secret Here]"

        use ExLineBotSdk.LineClient
      end

  ## Usages

  ### Verifying Line Requests

  In order to use this library to verify the line requests that came in.
  **Line requests must passed through plug `PlugSetRequestRawData` so that the raw request is saved in `conn.private[:raw_request_data]`**
  since we need raw Line's raw request data to verify that they are actually came from Line.

  To see how we can configure `PlugSetRequestRawData` checkout it usage documentation in [https://hexdocs.pm/plug_set_request_raw_data/PlugSetRequestRawData.html#module-usage-instruction](https://hexdocs.pm/plug_set_request_raw_data/PlugSetRequestRawData.html#module-usage-instruction)

  Below code show an example of how to configure `PlugSetRequestRawData` to set `conn.private[:raw_request_data]` when there is `x-line-signature` in request headers in an phoenix app.

      defmodule MyAppWeb.Endpoint do
        use Phoenix.Endpoint, otp_app: :my_app

        alias MyAppWeb.SetRawData

        ...

        plug(PlugSetRequestRawData, %{check: %{check: &SetRawData.check/1}) # Make sure that it is before Plug.Parsers plug

        plug(
          Plug.Parsers,
          parsers: [:urlencoded, :multipart, :json],
          pass: ["*/*"],
          json_decoder: Poison
        )
        ...
      end

      defmodule MyAppWeb.SetRawData do
        alias Plug.Conn

        def check(%Conn{} = conn) do
          conn.req_headers
          |> Enum.any?(fn {header, _content} -> header == "x-line-signature" end)
        end
      end

  Once `PlugSetRequestRawData` is properly configured like above code sample you can use `ExLineBotSdk.LineClient.verify/1` to check the validity of Line webhook requests.
  """

  alias Plug.Conn

  @doc false
  defmacro __using__(_) do
    quote do
      @behaviour ExLineBotSdk.LineClient

      def channel_secret do
        @channel_secret
      end

      def channel_access_token do
        @channel_access_token
      end

      def verify(conn) do
        signature_should_be = signature_for_request_should_be(conn)

        if Enum.any?(conn.req_headers, fn {header, content} ->
             {header, content} == {"x-line-signature", signature_should_be}
           end) do
          {:ok, conn}
        else
          {:error, "Line Request is not verified."}
        end
      end

      defp raw_request_data(conn) do
        conn.private[:raw_request_data] || ""
      end

      def signature_for_request_should_be(conn) do
        body = raw_request_data(conn)

        :crypto.hmac(:sha256, channel_secret(), body)
        |> Base.encode64()
      end
    end
  end

  @doc false
  @callback channel_secret() :: String.t()

  @doc false
  @callback channel_access_token() :: String.t()

  @doc """
  Verify if a Line's webhook post request is actually coming from Line.

  ## Example

        ExLineBotSdk.LineClient.veriy(conn)
        {:ok, conn} When Line request is verified else return {:error, "Line Request is not verified."}
  """
  @callback verify(%Conn{}) :: {:ok, %Conn{}} | {:error, String.t()}

  @doc false
  @callback signature_for_request_should_be(%Conn{}) :: String.t()
end
