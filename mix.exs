defmodule ExLineBotSdk.MixProject do
  use Mix.Project

  @version "0.0.8"

  def project do
    [
      app: :ex_line_bot_sdk,
      version: @version,
      elixir: "~> 1.4",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      source_url: "https://gitlab.com/blisscs/ex-line-bot-sdk",
      package: package(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ],
      docs: docs()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.18", only: :dev},
      {:excoveralls, "~> 0.8", only: :test},
      {:credo, "~> 0.9.3", only: [:dev, :test], runtime: false},
      {:plug, "~> 1.6.0"},
      {:plug_set_request_raw_data, "~> 0.1.0", only: [:test]}
    ]
  end

  defp package do
    [
      name: "ex_line_bot_sdk",
      maintainers: [
        "Suracheth Chawla"
      ],
      description: "Elixir Line Bot SDK",
      licenses: ["MIT"],
      links: %{gitlab: "https://gitlab.com/blisscs/ex-line-bot-sdk"}
    ]
  end

  defp docs do
    [main: "ExLineBotSdk"]
  end
end
